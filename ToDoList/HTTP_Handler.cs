﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;

namespace ToDoList
{
    public static class HTTP_Handler
    {
        public static readonly HttpClient client = new HttpClient();
        public async static Task<string> LogIn(string log, string pass)
        {
            var values = new Dictionary<string, string>
              {
                  { "username", log },
                  { "password", pass }
              };

            var content = new FormUrlEncodedContent(values);

            var response = await client.PostAsync("https://todo-web-part.onrender.com/login", content);

            var responseString = await response.Content.ReadAsStringAsync();
            JSONResponse js = JsonConvert.DeserializeObject<JSONResponse>(responseString);
            if (js.status.ToString() == "Access accepted")
            {
                return "good";
            }
            else if (js.status.ToString() == "No such user")
            {
                return "*Такого користувача не існує";
            }
            else if (js.status.ToString() == "Wrong password")
            {
                return "*Невірний пароль";
            }
            return "error";
        }
        public async static void addPoint(string username)
        {
            if (username.Length >= 4 & username.Length >= 6)
            {
                var values = new Dictionary<string, string>
              {
                  { "username", username }
              };

                var content = new FormUrlEncodedContent(values);

                var response = await client.PostAsync("https://todo-web-part.onrender.com/point", content);
            }

        }
        public async static Task<string> Register(string reg_name, string pass)
        {
            if (reg_name.Length >= 4 & pass.Length >= 6)
            {
                var values = new Dictionary<string, string>
              {
                  { "username", reg_name },
                  { "password", pass }
              };

                var content = new FormUrlEncodedContent(values);

                var response = await client.PostAsync("https://todo-web-part.onrender.com/register", content);

                var responseString = await response.Content.ReadAsStringAsync();
                JSONResponse js = JsonConvert.DeserializeObject<JSONResponse>(responseString);
                if (js.status.ToString() == "Such user already exists")
                {
                    return "*Користувач з таким іменем вже зареєстрований";
                }
                else if (js.status.ToString() == "User created")
                {
                    return "good";
                }
            }
            else
            {
                return "*Ім'я користувача повинно бути >= 4 символів, а пароль >= 6";
            }
            return "error";
        }
        public async static Task<User[]> GetLeaders()
        {
            User[] users = new User[10];
            var json = await client.GetStringAsync("https://todo-web-part.onrender.com/leaders");
            var des_resp = JsonConvert.DeserializeObject<Whip>(json);
            var des_user = JsonConvert.DeserializeObject<User[]>(des_resp.response);
            int i = 0;
            foreach( var user_d in des_user )
            {
                users[i] = new User ( user_d.username, user_d.tasks );
                i++;
            }
            return users;
        }
    }
}
