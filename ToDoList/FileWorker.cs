﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ToDoList
{
    public class FileWorker
    {
        public static void CreateFile(bool auto, string username, string password)
        {
            string file_path = AppDomain.CurrentDomain.BaseDirectory + "auto_login.dat";
            StreamWriter sw = new StreamWriter(file_path);
            sw.WriteLine(auto);
            sw.WriteLine(username);
            sw.WriteLine(password);
            sw.Close();
        }
        public static string[] CheckFile()
        {
            string file_path = AppDomain.CurrentDomain.BaseDirectory + "auto_login.dat";
            string[] res = new string[3];
            if (File.Exists(file_path))
            {
                StreamReader sr = new StreamReader(file_path);
                int i = 0;
                while (!sr.EndOfStream)
                {
                    res[i] = sr.ReadLine();
                    i++;
                }
                sr.Close();
            }
            return res;
        }
    }
}
