﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.TrackBar;

namespace ToDoList
{
    public class Whip
    {
        public string response { get; set; }
    }
    public class User
    {
        public string username { get; set; }
        public int tasks { get; set; }
        public User(string username, int tasks) { 
            this.tasks = tasks;
            this.username = username;
        }
    }
    class JSONResponse
    {
        public string status { get; set; }
    }
}
