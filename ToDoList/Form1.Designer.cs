﻿namespace ToDoList
{
    partial class LogIn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LogIn));
            this.login_panel = new System.Windows.Forms.Panel();
            this.log_help_label = new System.Windows.Forms.Label();
            this.log_to_reg_link = new System.Windows.Forms.Label();
            this.LogInBtn = new System.Windows.Forms.Button();
            this.PasswordField = new System.Windows.Forms.TextBox();
            this.LogInField = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.register_panel = new System.Windows.Forms.Panel();
            this.reg_help_label = new System.Windows.Forms.Label();
            this.reg_to_log_link = new System.Windows.Forms.Label();
            this.reg_btn = new System.Windows.Forms.Button();
            this.pass_reg_field = new System.Windows.Forms.TextBox();
            this.registerField = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label6 = new System.Windows.Forms.Label();
            this.MenuPanel = new System.Windows.Forms.Panel();
            this.log_out_btn = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.list_panel = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.label8 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.high_prio = new System.Windows.Forms.Button();
            this.low_prio = new System.Windows.Forms.Button();
            this.med_prio = new System.Windows.Forms.Button();
            this.create_btn = new System.Windows.Forms.Button();
            this.create_to_do = new System.Windows.Forms.TextBox();
            this.auto_login = new System.Windows.Forms.CheckBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.ses_name = new System.Windows.Forms.Label();
            this.login_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.register_panel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.MenuPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.list_panel.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // login_panel
            // 
            this.login_panel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.login_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(53)))), ((int)(((byte)(87)))));
            this.login_panel.Controls.Add(this.auto_login);
            this.login_panel.Controls.Add(this.log_help_label);
            this.login_panel.Controls.Add(this.log_to_reg_link);
            this.login_panel.Controls.Add(this.LogInBtn);
            this.login_panel.Controls.Add(this.PasswordField);
            this.login_panel.Controls.Add(this.LogInField);
            this.login_panel.Controls.Add(this.label3);
            this.login_panel.Controls.Add(this.label2);
            this.login_panel.Controls.Add(this.pictureBox1);
            this.login_panel.Controls.Add(this.label1);
            this.login_panel.Location = new System.Drawing.Point(270, 256);
            this.login_panel.Margin = new System.Windows.Forms.Padding(0);
            this.login_panel.Name = "login_panel";
            this.login_panel.Size = new System.Drawing.Size(900, 512);
            this.login_panel.TabIndex = 0;
            // 
            // log_help_label
            // 
            this.log_help_label.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(238)))));
            this.log_help_label.Location = new System.Drawing.Point(300, 380);
            this.log_help_label.Name = "log_help_label";
            this.log_help_label.Size = new System.Drawing.Size(500, 29);
            this.log_help_label.TabIndex = 9;
            // 
            // log_to_reg_link
            // 
            this.log_to_reg_link.Font = new System.Drawing.Font("Montserrat", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.log_to_reg_link.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.log_to_reg_link.Location = new System.Drawing.Point(431, 467);
            this.log_to_reg_link.Margin = new System.Windows.Forms.Padding(0);
            this.log_to_reg_link.Name = "log_to_reg_link";
            this.log_to_reg_link.Size = new System.Drawing.Size(469, 40);
            this.log_to_reg_link.TabIndex = 7;
            this.log_to_reg_link.Text = "Нема акаунту ? Зареєструйтесь";
            this.log_to_reg_link.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.log_to_reg_link.Click += new System.EventHandler(this.log_to_reg_link_Click);
            // 
            // LogInBtn
            // 
            this.LogInBtn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(238)))));
            this.LogInBtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LogInBtn.Font = new System.Drawing.Font("Montserrat", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LogInBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(123)))), ((int)(((byte)(157)))));
            this.LogInBtn.Location = new System.Drawing.Point(450, 300);
            this.LogInBtn.Margin = new System.Windows.Forms.Padding(0);
            this.LogInBtn.Name = "LogInBtn";
            this.LogInBtn.Size = new System.Drawing.Size(200, 50);
            this.LogInBtn.TabIndex = 6;
            this.LogInBtn.Text = "Login";
            this.LogInBtn.UseVisualStyleBackColor = false;
            this.LogInBtn.Click += new System.EventHandler(this.LogInBtn_Click);
            // 
            // PasswordField
            // 
            this.PasswordField.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(238)))));
            this.PasswordField.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PasswordField.Font = new System.Drawing.Font("Montserrat", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PasswordField.ForeColor = System.Drawing.Color.Black;
            this.PasswordField.Location = new System.Drawing.Point(300, 225);
            this.PasswordField.Margin = new System.Windows.Forms.Padding(0);
            this.PasswordField.Name = "PasswordField";
            this.PasswordField.PasswordChar = '*';
            this.PasswordField.Size = new System.Drawing.Size(500, 31);
            this.PasswordField.TabIndex = 5;
            this.PasswordField.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // LogInField
            // 
            this.LogInField.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(238)))));
            this.LogInField.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LogInField.Font = new System.Drawing.Font("Montserrat", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LogInField.ForeColor = System.Drawing.Color.Black;
            this.LogInField.Location = new System.Drawing.Point(300, 150);
            this.LogInField.Margin = new System.Windows.Forms.Padding(0);
            this.LogInField.Name = "LogInField";
            this.LogInField.Size = new System.Drawing.Size(500, 31);
            this.LogInField.TabIndex = 4;
            this.LogInField.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Montserrat", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.label3.Location = new System.Drawing.Point(300, 25);
            this.label3.Margin = new System.Windows.Forms.Padding(0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(500, 64);
            this.label3.TabIndex = 3;
            this.label3.Text = "Log In";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Montserrat", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.label2.Location = new System.Drawing.Point(0, 150);
            this.label2.Margin = new System.Windows.Forms.Padding(0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(200, 120);
            this.label2.TabIndex = 1;
            this.label2.Text = "To Do List";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(50, 25);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 100);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.Font = new System.Drawing.Font("Montserrat", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.label1.Location = new System.Drawing.Point(0, 462);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(250, 50);
            this.label1.TabIndex = 1;
            this.label1.Text = "Made by Sasha Hal";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.White;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.button1.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(1045, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(0);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(60, 40);
            this.button1.TabIndex = 1;
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.White;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.button2.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(1105, 0);
            this.button2.Margin = new System.Windows.Forms.Padding(0);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(60, 40);
            this.button2.TabIndex = 2;
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.White;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.button3.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Image = ((System.Drawing.Image)(resources.GetObject("button3.Image")));
            this.button3.Location = new System.Drawing.Point(1165, 0);
            this.button3.Margin = new System.Windows.Forms.Padding(0);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(60, 40);
            this.button3.TabIndex = 3;
            this.button3.Text = "\r\n";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Location = new System.Drawing.Point(199, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1225, 40);
            this.panel2.TabIndex = 4;
            // 
            // register_panel
            // 
            this.register_panel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.register_panel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(53)))), ((int)(((byte)(87)))));
            this.register_panel.Controls.Add(this.reg_help_label);
            this.register_panel.Controls.Add(this.reg_to_log_link);
            this.register_panel.Controls.Add(this.reg_btn);
            this.register_panel.Controls.Add(this.pass_reg_field);
            this.register_panel.Controls.Add(this.registerField);
            this.register_panel.Controls.Add(this.label4);
            this.register_panel.Controls.Add(this.label5);
            this.register_panel.Controls.Add(this.pictureBox2);
            this.register_panel.Controls.Add(this.label6);
            this.register_panel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(238)))));
            this.register_panel.Location = new System.Drawing.Point(270, 256);
            this.register_panel.Margin = new System.Windows.Forms.Padding(0);
            this.register_panel.Name = "register_panel";
            this.register_panel.Size = new System.Drawing.Size(900, 512);
            this.register_panel.TabIndex = 5;
            // 
            // reg_help_label
            // 
            this.reg_help_label.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(238)))));
            this.reg_help_label.Location = new System.Drawing.Point(300, 380);
            this.reg_help_label.Name = "reg_help_label";
            this.reg_help_label.Size = new System.Drawing.Size(500, 30);
            this.reg_help_label.TabIndex = 7;
            // 
            // reg_to_log_link
            // 
            this.reg_to_log_link.Font = new System.Drawing.Font("Montserrat", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.reg_to_log_link.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.reg_to_log_link.Location = new System.Drawing.Point(431, 467);
            this.reg_to_log_link.Margin = new System.Windows.Forms.Padding(0);
            this.reg_to_log_link.Name = "reg_to_log_link";
            this.reg_to_log_link.Size = new System.Drawing.Size(469, 40);
            this.reg_to_log_link.TabIndex = 8;
            this.reg_to_log_link.Text = "Існує акаунт ? Авторизуйтесь";
            this.reg_to_log_link.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.reg_to_log_link.Click += new System.EventHandler(this.reg_to_log_link_Click);
            // 
            // reg_btn
            // 
            this.reg_btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(238)))));
            this.reg_btn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.reg_btn.Font = new System.Drawing.Font("Montserrat", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.reg_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(123)))), ((int)(((byte)(157)))));
            this.reg_btn.Location = new System.Drawing.Point(450, 300);
            this.reg_btn.Margin = new System.Windows.Forms.Padding(0);
            this.reg_btn.Name = "reg_btn";
            this.reg_btn.Size = new System.Drawing.Size(200, 50);
            this.reg_btn.TabIndex = 6;
            this.reg_btn.Text = "Register";
            this.reg_btn.UseVisualStyleBackColor = false;
            this.reg_btn.Click += new System.EventHandler(this.reg_btn_Click);
            // 
            // pass_reg_field
            // 
            this.pass_reg_field.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(238)))));
            this.pass_reg_field.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pass_reg_field.Font = new System.Drawing.Font("Montserrat", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.pass_reg_field.ForeColor = System.Drawing.Color.Black;
            this.pass_reg_field.Location = new System.Drawing.Point(300, 225);
            this.pass_reg_field.Margin = new System.Windows.Forms.Padding(0);
            this.pass_reg_field.Name = "pass_reg_field";
            this.pass_reg_field.PasswordChar = '*';
            this.pass_reg_field.Size = new System.Drawing.Size(500, 31);
            this.pass_reg_field.TabIndex = 5;
            this.pass_reg_field.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // registerField
            // 
            this.registerField.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(238)))));
            this.registerField.Cursor = System.Windows.Forms.Cursors.Hand;
            this.registerField.Font = new System.Drawing.Font("Montserrat", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.registerField.ForeColor = System.Drawing.Color.Black;
            this.registerField.Location = new System.Drawing.Point(300, 150);
            this.registerField.Margin = new System.Windows.Forms.Padding(0);
            this.registerField.Name = "registerField";
            this.registerField.Size = new System.Drawing.Size(500, 31);
            this.registerField.TabIndex = 4;
            this.registerField.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Montserrat", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.label4.Location = new System.Drawing.Point(300, 25);
            this.label4.Margin = new System.Windows.Forms.Padding(0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(500, 64);
            this.label4.TabIndex = 3;
            this.label4.Text = "Register";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Montserrat", 32.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.label5.Location = new System.Drawing.Point(0, 150);
            this.label5.Margin = new System.Windows.Forms.Padding(0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(200, 120);
            this.label5.TabIndex = 1;
            this.label5.Text = "To Do List";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(50, 25);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 100);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 2;
            this.pictureBox2.TabStop = false;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.Font = new System.Drawing.Font("Montserrat", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.label6.Location = new System.Drawing.Point(0, 462);
            this.label6.Margin = new System.Windows.Forms.Padding(0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(250, 50);
            this.label6.TabIndex = 1;
            this.label6.Text = "Made by Sasha Hal";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MenuPanel
            // 
            this.MenuPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(29)))), ((int)(((byte)(53)))), ((int)(((byte)(87)))));
            this.MenuPanel.Controls.Add(this.ses_name);
            this.MenuPanel.Controls.Add(this.log_out_btn);
            this.MenuPanel.Controls.Add(this.button5);
            this.MenuPanel.Controls.Add(this.button4);
            this.MenuPanel.Controls.Add(this.label7);
            this.MenuPanel.Controls.Add(this.pictureBox3);
            this.MenuPanel.Dock = System.Windows.Forms.DockStyle.Left;
            this.MenuPanel.Location = new System.Drawing.Point(0, 0);
            this.MenuPanel.Margin = new System.Windows.Forms.Padding(0);
            this.MenuPanel.Name = "MenuPanel";
            this.MenuPanel.Size = new System.Drawing.Size(200, 985);
            this.MenuPanel.TabIndex = 6;
            // 
            // log_out_btn
            // 
            this.log_out_btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(238)))));
            this.log_out_btn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.log_out_btn.Font = new System.Drawing.Font("Montserrat", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.log_out_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(123)))), ((int)(((byte)(157)))));
            this.log_out_btn.Location = new System.Drawing.Point(25, 900);
            this.log_out_btn.Margin = new System.Windows.Forms.Padding(0);
            this.log_out_btn.Name = "log_out_btn";
            this.log_out_btn.Size = new System.Drawing.Size(150, 50);
            this.log_out_btn.TabIndex = 10;
            this.log_out_btn.Text = "Вийти";
            this.log_out_btn.UseVisualStyleBackColor = false;
            this.log_out_btn.Click += new System.EventHandler(this.log_out_btn_Click);
            // 
            // button5
            // 
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Montserrat", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.button5.Location = new System.Drawing.Point(25, 350);
            this.button5.Margin = new System.Windows.Forms.Padding(0);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(150, 50);
            this.button5.TabIndex = 9;
            this.button5.Text = "Лідери";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Montserrat", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.button4.Location = new System.Drawing.Point(25, 300);
            this.button4.Margin = new System.Windows.Forms.Padding(0);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(150, 50);
            this.button4.TabIndex = 8;
            this.button4.Text = "Завдання";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Montserrat", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.label7.Location = new System.Drawing.Point(25, 150);
            this.label7.Margin = new System.Windows.Forms.Padding(0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(150, 100);
            this.label7.TabIndex = 7;
            this.label7.Text = "To Do List";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(50, 25);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(100, 100);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // list_panel
            // 
            this.list_panel.Controls.Add(this.label9);
            this.list_panel.Controls.Add(this.flowLayoutPanel1);
            this.list_panel.Controls.Add(this.label8);
            this.list_panel.Controls.Add(this.panel1);
            this.list_panel.Controls.Add(this.create_btn);
            this.list_panel.Controls.Add(this.create_to_do);
            this.list_panel.Location = new System.Drawing.Point(201, 40);
            this.list_panel.Margin = new System.Windows.Forms.Padding(0);
            this.list_panel.Name = "list_panel";
            this.list_panel.Size = new System.Drawing.Size(1224, 945);
            this.list_panel.TabIndex = 7;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Montserrat", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.label9.Location = new System.Drawing.Point(560, 47);
            this.label9.Margin = new System.Windows.Forms.Padding(0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(250, 50);
            this.label9.TabIndex = 5;
            this.label9.Text = "Пріоритетність";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.AutoScroll = true;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(100, 195);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(875, 670);
            this.flowLayoutPanel1.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Montserrat", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.label8.Location = new System.Drawing.Point(100, 150);
            this.label8.Margin = new System.Windows.Forms.Padding(0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(250, 42);
            this.label8.TabIndex = 3;
            this.label8.Text = "Завдання";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.panel1.Controls.Add(this.high_prio);
            this.panel1.Controls.Add(this.low_prio);
            this.panel1.Controls.Add(this.med_prio);
            this.panel1.Location = new System.Drawing.Point(813, 47);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(140, 50);
            this.panel1.TabIndex = 2;
            // 
            // high_prio
            // 
            this.high_prio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.high_prio.FlatAppearance.BorderSize = 0;
            this.high_prio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.high_prio.Image = ((System.Drawing.Image)(resources.GetObject("high_prio.Image")));
            this.high_prio.Location = new System.Drawing.Point(95, 5);
            this.high_prio.Margin = new System.Windows.Forms.Padding(0);
            this.high_prio.Name = "high_prio";
            this.high_prio.Size = new System.Drawing.Size(40, 40);
            this.high_prio.TabIndex = 2;
            this.high_prio.UseVisualStyleBackColor = true;
            this.high_prio.Click += new System.EventHandler(this.high_prio_Click);
            // 
            // low_prio
            // 
            this.low_prio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.low_prio.FlatAppearance.BorderSize = 0;
            this.low_prio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.low_prio.Image = ((System.Drawing.Image)(resources.GetObject("low_prio.Image")));
            this.low_prio.Location = new System.Drawing.Point(5, 5);
            this.low_prio.Margin = new System.Windows.Forms.Padding(0);
            this.low_prio.Name = "low_prio";
            this.low_prio.Size = new System.Drawing.Size(40, 40);
            this.low_prio.TabIndex = 0;
            this.low_prio.UseVisualStyleBackColor = true;
            this.low_prio.Click += new System.EventHandler(this.low_prio_Click);
            // 
            // med_prio
            // 
            this.med_prio.Cursor = System.Windows.Forms.Cursors.Hand;
            this.med_prio.FlatAppearance.BorderSize = 0;
            this.med_prio.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.med_prio.Image = ((System.Drawing.Image)(resources.GetObject("med_prio.Image")));
            this.med_prio.Location = new System.Drawing.Point(50, 5);
            this.med_prio.Margin = new System.Windows.Forms.Padding(0);
            this.med_prio.Name = "med_prio";
            this.med_prio.Size = new System.Drawing.Size(40, 40);
            this.med_prio.TabIndex = 1;
            this.med_prio.UseVisualStyleBackColor = true;
            this.med_prio.Click += new System.EventHandler(this.med_prio_Click);
            // 
            // create_btn
            // 
            this.create_btn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(230)))), ((int)(((byte)(57)))), ((int)(((byte)(70)))));
            this.create_btn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.create_btn.FlatAppearance.BorderSize = 0;
            this.create_btn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.create_btn.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.create_btn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.create_btn.Location = new System.Drawing.Point(720, 100);
            this.create_btn.Margin = new System.Windows.Forms.Padding(0);
            this.create_btn.Name = "create_btn";
            this.create_btn.Size = new System.Drawing.Size(90, 33);
            this.create_btn.TabIndex = 1;
            this.create_btn.Text = "Додати";
            this.create_btn.UseVisualStyleBackColor = false;
            this.create_btn.Click += new System.EventHandler(this.create_btn_Click);
            // 
            // create_to_do
            // 
            this.create_to_do.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(250)))));
            this.create_to_do.Font = new System.Drawing.Font("Montserrat", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.create_to_do.Location = new System.Drawing.Point(100, 100);
            this.create_to_do.Name = "create_to_do";
            this.create_to_do.Size = new System.Drawing.Size(620, 33);
            this.create_to_do.TabIndex = 0;
            // 
            // auto_login
            // 
            this.auto_login.Font = new System.Drawing.Font("Montserrat", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.auto_login.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(238)))));
            this.auto_login.Location = new System.Drawing.Point(300, 271);
            this.auto_login.Name = "auto_login";
            this.auto_login.Size = new System.Drawing.Size(265, 26);
            this.auto_login.TabIndex = 10;
            this.auto_login.Text = "Запам\'ятати мене";
            this.auto_login.UseVisualStyleBackColor = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "S";
            // 
            // ses_name
            // 
            this.ses_name.Font = new System.Drawing.Font("Montserrat", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ses_name.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(250)))), ((int)(((byte)(238)))));
            this.ses_name.Location = new System.Drawing.Point(30, 834);
            this.ses_name.Name = "ses_name";
            this.ses_name.Size = new System.Drawing.Size(145, 34);
            this.ses_name.TabIndex = 8;
            this.ses_name.Text = "Ім\'я користувача";
            this.ses_name.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LogIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(69)))), ((int)(((byte)(123)))), ((int)(((byte)(157)))));
            this.ClientSize = new System.Drawing.Size(1424, 985);
            this.Controls.Add(this.MenuPanel);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.list_panel);
            this.Controls.Add(this.register_panel);
            this.Controls.Add(this.login_panel);
            this.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.Font = new System.Drawing.Font("Montserrat", 8.249999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ForeColor = System.Drawing.Color.Black;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.Name = "LogIn";
            this.Text = "To Do List by Sasha Hal";
            this.Load += new System.EventHandler(this.LogIn_Load);
            this.login_panel.ResumeLayout(false);
            this.login_panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.register_panel.ResumeLayout(false);
            this.register_panel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.MenuPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.list_panel.ResumeLayout(false);
            this.list_panel.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel login_panel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox LogInField;
        private System.Windows.Forms.TextBox PasswordField;
        private System.Windows.Forms.Button LogInBtn;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel register_panel;
        private System.Windows.Forms.Button reg_btn;
        private System.Windows.Forms.TextBox pass_reg_field;
        private System.Windows.Forms.TextBox registerField;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label log_to_reg_link;
        private System.Windows.Forms.Label reg_to_log_link;
        private System.Windows.Forms.Panel MenuPanel;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button log_out_btn;
        private System.Windows.Forms.Label reg_help_label;
        private System.Windows.Forms.Label log_help_label;
        private System.Windows.Forms.Panel list_panel;
        private System.Windows.Forms.TextBox create_to_do;
        private System.Windows.Forms.Button create_btn;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button high_prio;
        private System.Windows.Forms.Button med_prio;
        private System.Windows.Forms.Button low_prio;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox auto_login;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label ses_name;
    }
}

