﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using Newtonsoft.Json;
using System.IO;

namespace ToDoList
{
    public partial class LogIn : Form
    {
        Panel[] pnl = new Panel[] { };
        int tasks_amount = 0;
        string[,] arr_tasks = new string[1,2];
        string[,] temp_arr_tasks = new string[1, 2];
        int current_prio = 0;//1 -> low| 2 -> medium | 3 -> High
        string temp_username = "";
        string temp_password = "";

        bool full_width = false;
        
        class JSONResponse
        {
            public string status { get; set; }
        }

        public LogIn()
        {
            InitializeComponent();
        }

        private async void LogInBtn_Click(object sender, EventArgs e) // Логін
        {
            string log = LogInField.Text;
            string pass = PasswordField.Text;
            string res = await HTTP_Handler.LogIn(log, pass);
            if(res == "good")
            {
                if (auto_login.Checked)
                {
                    FileWorker.CreateFile(true, log, pass);
                }
                else
                {
                    FileWorker.CreateFile(false, "none", "none");
                }
                temp_username = LogInField.Text;
                temp_password = PasswordField.Text;
                MenuPanel.Visible = true;
                login_panel.Visible = false;
                list_panel.Visible = true;
                ses_name.Text = temp_username;
            }
            else
            {
                log_help_label.Text = res;
            }
        }

        private void button1_Click(object sender, EventArgs e) // Мінімізувати вікно
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private async void LogIn_Load(object sender, EventArgs e) // При завантаженні форми, сховати границю та автоматичний логін
        {
            this.FormBorderStyle = FormBorderStyle.None;
            list_panel.Visible = false;
            login_panel.Visible = false;
            register_panel.Visible = true;
            MenuPanel.Visible = false;
            string[] fw = FileWorker.CheckFile();
            string log = fw[1];
            string pass = fw[2];
            if (Convert.ToBoolean(fw[0]))
            {
                string res = await HTTP_Handler.LogIn(log, pass);
                if (res == "good")
                {
                    temp_username = log;
                    temp_password = pass;
                    MenuPanel.Visible = true;
                    login_panel.Visible = false;
                    list_panel.Visible = true;
                    register_panel.Visible = false;
                    ses_name.Text = temp_username;
                }
                else
                {
                    log_help_label.Text = res;
                }
            }
        }

        private void button3_Click(object sender, EventArgs e) // Закрити вікно
        {
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e) // На весь екран вікно
        {
            if (!full_width)
            {
                this.WindowState = FormWindowState.Maximized;
                full_width = true;
            }
            else
            {
                this.WindowState = FormWindowState.Normal;
                full_width = false;
            }
        }

        private void log_to_reg_link_Click(object sender, EventArgs e) // Перенаправлення на форму реєстрування
        {
            register_panel.Visible = true;
            login_panel.Visible = false;
        }

        private void reg_to_log_link_Click(object sender, EventArgs e)// Перенаправлення на форму логіну
        {
            register_panel.Visible = false;
            login_panel.Visible = true;
        }

        private async void reg_btn_Click(object sender, EventArgs e) // Реєстрація
        {
            string reg_name = registerField.Text;
            string pass = pass_reg_field.Text;
            string res = await HTTP_Handler.Register(reg_name, pass);
            if(res == "good")
            {
                MessageBox.Show("Акаунт створено успішно");
                login_panel.Visible = true;
                register_panel.Visible = false;
            }
            else
            {
                reg_help_label.Text = res;
            }
        }

        private void log_out_btn_Click(object sender, EventArgs e) //Вийти з акаунту
        {
            MenuPanel.Visible = false;
            login_panel.Visible = true;
            temp_username = "";
            temp_password = "";
            list_panel.Visible = false;
        }

        private void create_btn_Click(object sender, EventArgs e)
        {
            string task_name = create_to_do.Text;
            create_to_do.Text = string.Empty;
            int task_prio = current_prio;
            current_prio = 0;
            temp_arr_tasks = arr_tasks;
            tasks_amount++;
            arr_tasks = new string[tasks_amount, 2];
            for (int i = 0; i < tasks_amount; i++)
            {
                if(i == tasks_amount - 1)
                {
                    arr_tasks[i, 0] = task_name;
                    arr_tasks[i, 1] = task_prio.ToString();
                }
                else
                {
                    arr_tasks[i, 0] = temp_arr_tasks[i, 0];
                    arr_tasks[i, 1] = temp_arr_tasks[i, 1];
                }
            }
            task_prio = 0;
            task_name= string.Empty;
            pnl = new Panel[tasks_amount];
            RenderTasks();
        }

        private void button8_Click(object sender, EventArgs e)
        {

        }

        private void low_prio_Click(object sender, EventArgs e) //1 -> low| 2 -> medium | 3 -> High
        {
            current_prio = 1;
        }

        private void med_prio_Click(object sender, EventArgs e)
        {
            current_prio = 2;
        }

        private void high_prio_Click(object sender, EventArgs e)
        {
            current_prio = 3;
        }

        public void RenderTasks()
        {
            int panel_y = 0;
            flowLayoutPanel1.Controls.Clear();
            for (int i = 0; i < tasks_amount; i++)
            {
                    Label l = new Label();
                    PictureBox pb = new PictureBox();
                    Button btn = new Button();
                    Button btn2 = new Button();
                    pnl[i] = new Panel();
                if (arr_tasks[i, 1] == "none")
                {
                    pnl[i].Visible = false;
                }
                else { 
                        if (Convert.ToInt32(arr_tasks[i, 1]) > 0)
                        {
                            pb.Left = 600;
                            pb.Top = 25;
                            pb.Size = new Size(50, 50);
                            if (Convert.ToInt32(arr_tasks[i, 1]) == 1)
                            {
                                pb.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "low_prio.png");
                            }
                            else if (Convert.ToInt32(arr_tasks[i, 1]) == 2)
                            {
                                pb.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "mid_prio.png");
                            }
                            else
                            {
                                pb.Image = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "high_prio.png");
                            }
                            pb.SizeMode = PictureBoxSizeMode.CenterImage;
                            pb.BackgroundImageLayout = ImageLayout.Center;
                            pnl[i].Controls.Add(pb);
                        }
                    }
                    pb.Left = 600;
                    pb.Top = 25;
                    btn.Left = 700;
                    btn.Top = 25;
                    btn.Size = new Size(50, 50);
                    btn.BackgroundImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "tick.png");
                    btn.BackgroundImageLayout = ImageLayout.Center;
                    btn.FlatStyle = FlatStyle.Flat;
                    btn.FlatAppearance.BorderSize = 0;
                    btn2.Left = 775;
                    btn2.Top = 25;
                    btn2.Size = new Size(50, 50);
                    btn2.BackgroundImage = Image.FromFile(AppDomain.CurrentDomain.BaseDirectory + "cross.png");
                    btn2.BackgroundImageLayout = ImageLayout.Center;
                    btn2.FlatStyle = FlatStyle.Flat;
                    btn2.FlatAppearance.BorderSize = 0;
                    pb.Size = new Size(50, 50);
                    btn.Click += new EventHandler(Complete_Task);
                    pnl[i].Tag = i;
                    btn2.Click += new EventHandler(Delete_Task);
                    pnl[i].Controls.Add(btn);
                    pnl[i].Controls.Add(btn2);
                    l.Text = arr_tasks[i, 0];
                    l.AutoSize = false;
                    l.ForeColor = Color.FromArgb(29, 53, 87);
                    l.Top = 25;
                    l.Size = new Size(500, 50);
                    l.Left = 25;
                    l.Font = new Font("Montserrat", 12, FontStyle.Bold);
                    pnl[i].Controls.Add(l);
                    pnl[i].BackColor = Color.FromArgb(241, 250, 250);
                    pnl[i].Size = new Size(850, 100);
                    pnl[i].BorderStyle = BorderStyle.FixedSingle;
                    pnl[i].Location = new Point(0, panel_y);
                    panel_y += 150;
                    flowLayoutPanel1.Controls.Add(pnl[i]);
            }
        }

        private async void button5_Click(object sender, EventArgs e)
        {
            User[] res = await HTTP_Handler.GetLeaders();
            foreach(User user in res)
            {
                MessageBox.Show(user.username + " " + user.tasks);
            }
        }
        public async void Complete_Task(object sender, EventArgs e)
        {
            Button clicked_btn = (Button)sender;
            Control parentControl = clicked_btn.Parent;
            int tag_int = (int)parentControl.Tag;
            arr_tasks[tag_int, 0] = "none";
            arr_tasks[tag_int, 1] = "none";
            HTTP_Handler.addPoint(temp_username);
            parentControl.Visible= false;
            RenderTasks();
        }
        protected void Delete_Task(object sender, EventArgs e)
        {
            Button clicked_btn = (Button)sender;
            Control parentControl = clicked_btn.Parent;
            int tag_int = (int)parentControl.Tag;
            arr_tasks[tag_int, 0] = "none";
            arr_tasks[tag_int, 1] = "none";
            parentControl.Visible = false;
            RenderTasks();
        }
    }
}
